
  describe('Form', () => {
    
    it('checks if client works', () => {
        cy.visit('http://localhost:5500')
      })
  
      it('Check green class according to first state', () => {
        const input = "11111"
        cy.get('input[name=0]')
          .type(input)
        cy.get('input[name=1]')
            .type(input)
        cy.get('input[name=2]')
            .type(input)
        cy.get('input[name=3]')
            .type(input)
        cy.get('input[name=4]')
            .type(input)
        cy.get('input[name=5]').click()

        cy.get('table').get('td').eq(2).should('have.class', 'cell')
        cy.get('table').get('td').eq(0).should('have.class', 'cell')
        cy.get('table').get('td').eq(5).should('have.class', 'cell')
            
      })




      it('Check null class according to first state', () => {

        cy.get('input[name=0]').clear() 
        cy.get('input[name=1]').clear() 
        cy.get('input[name=2]').clear() 
        cy.get('input[name=3]').clear()
        cy.get('input[name=4]').clear()  

        const input = "00000"
        cy.get('input[name=0]')
          .type(input)
        cy.get('input[name=1]')
            .type(input)
        cy.get('input[name=2]')
            .type(input)
        cy.get('input[name=3]')
            .type(input)
        cy.get('input[name=4]')
            .type(input)
        cy.get('input[name=5]').click()

        cy.get('table').get('td').eq(2).should('have.class', '')
        cy.get('table').get('td').eq(0).should('have.class', '')
        cy.get('table').get('td').eq(5).should('have.class', '')
            
      })

      it('checks form submit', () => {
        cy.get('input[name=0]').clear() 
        cy.get('input[name=1]').clear() 
        cy.get('input[name=2]').clear() 
        cy.get('input[name=3]').clear()
        cy.get('input[name=4]').clear()  

        const input = "00000"
        cy.get('input[name=0]')
          .type(input)
        cy.get('input[name=1]')
            .type(input)
        cy.get('input[name=2]')
            .type(input)
        cy.get('input[name=3]')
            .type(input)
        cy.get('input[name=4]')
            .type(input)
        cy.get('input[name=5]').click()
        cy.get('#form1').submit()
      })




  
      it('Checks the change of the state on the UI after Interval', () => {

        cy.get('input[name=0]').clear() 
        cy.get('input[name=1]').clear() 
        cy.get('input[name=2]').clear() 
        cy.get('input[name=3]').clear()
        cy.get('input[name=4]').clear()  

        const input = "11111"
        cy.get('input[name=0]')
          .type(input)
        cy.get('input[name=1]')
            .type(input)
        cy.get('input[name=2]')
            .type(input)
        cy.get('input[name=3]')
            .type(input)
        cy.get('input[name=4]')
            .type(input)
        cy.get('input[name=5]').click().wait(3000).get('table').get('td').eq(1).should('have.class', '')

        

            
      })
  })