const { React, ReactDOM } = window;
// import './index.css'

class Home extends React.Component {

  constructor(props) {
    super(props);
 
    this.state = [
      [0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0],
      [0, 0, 0, 0, 0]
    ];

    this.dataInputs = {
      0: [],
      1: [],
      2: [],
      3: [],
      4: []
    };

    this.data = [
      [],
      [],
      [],
      [],
      []
    ];

    this.checkChange = false;
    
  }


  changeHandler = e => {
    this.dataInputs[e.target.name] = e.target.value;


  };

  submitHandler = e => {

    e.preventDefault();

    for(var i=0; i<5; i++){
      var arrayTemp = [];
      for(var x=0; x<5; x++){
        arrayTemp.push(parseInt(this.dataInputs[i].charAt(x)));
      }
      this.data[i] = arrayTemp;
    }


  
    e.preventDefault();
    fetch("http://localhost:8080/life", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.data)
    })
      .then(response => response.json())
      .then(data => {
   
        this.setState(data);
  
        this.checkChange = true;

      })
      .catch(error => {
        console.error("Error:", error);
      });
  };

  componentDidMount() {
    
      this.interval = setInterval(() => {

        if(this.checkChange){
          fetch("http://localhost:8080/life/next", {
            method: "POST",
            headers: {
              "Content-Type": "application/json"
            },
            body:[]
          })
            .then(response => response.json())
            .then(data => {
              console.log(data);
              this.setState(data);
            })
            .catch(error => {
              console.error("Error:", error);
            });
        }

         
       }, 3000); 
    
  }
  componentWillUnmount() {
    clearInterval(this.interval);
    
  }

















  render() {
    return (
      <div className="Home">


        <form id="form1" onSubmit={this.submitHandler}>
            <input type="text" name="0" pattern="[0-1]+" minlength="5" minlength="5" required onChange={this.changeHandler}/>
            <input type="text" name="1" pattern="[0-1]+" minlength="5" minlength="5" required onChange={this.changeHandler}/>
            <input type="text" name="2" pattern="[0-1]+" minlength="5" minlength="5" required onChange={this.changeHandler}/>
            <input type="text" name="3" pattern="[0-1]+" minlength="5" minlength="5" required onChange={this.changeHandler}/>
            <input type="text" name="4" pattern="[0-1]+" minlength="5" minlength="5" required onChange={this.changeHandler}/>

            <input name="5" type="submit" value="Set First State" />
        </form>


        <table>
          <tr>
          {this.state[0].map((item, index) => (
            <td className={item==1 ? 'cell' : null}>{item}</td>
          ))}
          </tr>
          <tr>
          {this.state[1].map((item, index) => (
            <td className={item==1 ? 'cell' : null}>{item}</td>
          ))}
          </tr>
          <tr>
          {this.state[2].map((item, index) => (
            <td className={item==1 ? 'cell' : null}>{item}</td>
          ))}
          </tr>
          <tr>
          {this.state[3].map((item, index) => (
            <td className={item==1 ? 'cell' : null}>{item}</td>
          ))}
          </tr>
          <tr>
          {this.state[4].map((item, index) => (
            <td className={item==1 ? 'cell' : null}>{item}</td>
          ))}
          </tr>
          
        </table>








      </div>
      
    )
  }
}

ReactDOM.render(<Home />, document.getElementById('root'))